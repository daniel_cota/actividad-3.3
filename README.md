# Problemas de programación Basica
***
Trabajo elaborado para la materia "Desarrollo basado en plataformas"

## Profesor: I.S.C. Luis Antonio Ramirez Martinez

### Requisitos Previos

Para realizar esta tarea fue necesario tener conocimientos basicos acerca de los temas visto en la materia, como lo son:

```
    Tener una partición con Linux en tu Sistema operativo
    Conocimientos de Linux
    Conocimientos de Git
    Conocimientos de JavaScript
    Conocimiento de Node.js
```

### Requisitos del trabajo :ballot_box_with_check:
***

Vamos a aplicar nuestros conocimientos adquiridos de Javascript y  de Node.Js para programar un blockchain, para esto sigue los siguientes pasos:

1) Genera una clase Block como modelo con los siguientes atributos:

   a) Index = Identificador de la posición del bloque en la cadena.
   b) Data = El contenido del bloque
   c) previousHash = Valor del bloque anterior de la cadena

2) Genera una clase Blockchain.

3) Mediante la clase Blockchain manipula elementos Block para poder generar la cadena de bloques.

4) Genera un método mine que calcule el nuevo hash del bloque según su dificultad.

Sube la liga de gitlab con el proyecto generado.

## Autor

- Daniel Alberto Cota Ochoa     ***329701***
    - [Gitlab](https://gitlab.com/daniel_cota)
